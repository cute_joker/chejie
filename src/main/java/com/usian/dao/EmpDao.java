package com.usian.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.pojo.Dept;
import com.usian.pojo.Emp;
import com.usian.pojo.EmpVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpDao extends BaseMapper<Emp> {
    List<EmpVo> findAllEmp(String name);

    List<Dept> findAllDept();

    void addUser(Emp emp);

    Emp findById(Integer id);

    void edit(Emp emp);

    void del(Integer id);
}
