package com.usian.service;

import com.usian.dao.EmpDao;
import com.usian.pojo.Dept;
import com.usian.pojo.Emp;
import com.usian.pojo.EmpVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpService {
    @Autowired
    private EmpDao empDao;

    public List<EmpVo> findAllEmp(String name) {
        return empDao.findAllEmp(name);
    }

    public List<Dept> findAllDept() {
        return empDao.findAllDept();
    }

    public void addUser(Emp emp) {
        empDao.addUser(emp);
    }

    public Emp findById(Integer id) {
        return empDao.findById(id);
    }

    public void edit(Emp emp) {
        empDao.edit(emp);
    }

    public void del(Integer id) {
        empDao.del(id);
    }
}
