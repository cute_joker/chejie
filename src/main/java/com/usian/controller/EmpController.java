package com.usian.controller;

import com.usian.pojo.Dept;
import com.usian.pojo.Emp;
import com.usian.pojo.EmpVo;
import com.usian.service.EmpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("emp")
public class EmpController {
    private final static Logger logger = LoggerFactory.getLogger(EmpController.class);

    @Autowired
    private EmpService empService;

    @RequestMapping("show")
    public String show(Model model,String name){
        List<EmpVo> empList=empService.findAllEmp(name);
        model.addAttribute("empList",empList);
        model.addAttribute("name",name);
        logger.info("logback成功了!");
        logger.debug("logback成功了!");
        logger.error("logback成功了!");
        return "show";
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model){
        List<Dept> deptList=empService.findAllDept();
        model.addAttribute("deptList",deptList);
        return "add";
    }

    @RequestMapping("add")
    public String add(Emp emp){
        empService.addUser(emp);
        return "redirect:/emp/show";
    }

    @RequestMapping("findById/{id}")
    public String findById(@PathVariable Integer id,Model model){
        Emp emp=empService.findById(id);
        model.addAttribute("e",emp);
        List<Dept> deptList=empService.findAllDept();
        model.addAttribute("deptList",deptList);
        return "edit";
    }

    @RequestMapping("edit")
    public String edit(Emp emp){
        empService.edit(emp);
        return "redirect:/emp/show";
    }

    @RequestMapping("del/{id}")
    public String del(@PathVariable Integer id){
        empService.del(id);
        return "redirect:/emp/show";
    }
}
