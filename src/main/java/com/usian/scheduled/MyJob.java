package com.usian.scheduled;

import com.usian.utils.Testclass;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

public class MyJob implements Job {
    @Autowired
    private Testclass testclass;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        //System.out.println("Quartz...."+new Date());
        testclass.hello2();
    }
}
