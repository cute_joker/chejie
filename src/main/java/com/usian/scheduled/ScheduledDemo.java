package com.usian.scheduled;

import com.usian.utils.Testclass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduled 定时任务器：是 Spring3.0 以后自带的一个定时任务器。
 * */
@Component
public class ScheduledDemo {
    @Autowired
    private Testclass testclass;

    @Scheduled(cron="0/2 * * * * ?")
    public void test(){
        testclass.hello();
    }
}
